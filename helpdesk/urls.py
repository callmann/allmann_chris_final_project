from django.urls import path
from django.conf.urls import url
from helpdesk import views as core_views
from django.contrib.auth import views as auth_views
from helpdesk.views import *
from . import views



urlpatterns = [
     url(r'^$', core_views.home, name='home'),
     url(r'^signup/$', core_views.signup, name='signup'),
     url(r'^account_activation_sent/$', core_views.account_activation_sent, name='account_activation_sent'),
     url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
         core_views.activate, name='activate'),

    path('ticket/',
         TicketList.as_view(),
         name='helpdesk_ticket_list_urlpattern'),

    path('ticket/<int:pk>/',
         TicketDetail.as_view(),
         name='helpdesk_ticket_detail_urlpattern'),

    path('ticket/create/',
         TicketCreate.as_view(),
         name='helpdesk_ticket_create_urlpattern'
         ),

    path('ticket/<int:pk>/update/',
         TicketUpdate.as_view(),
         name='helpdesk_ticket_update_urlpattern'),

    path('ticket/<int:pk>/delete/',
         TicketDelete.as_view(),
         name='helpdesk_ticket_delete_urlpattern'),

    path('ticket/<int:pk>/comment',
         views.CommentCreate,
         name='helpdesk_comment_create_urlpattern'),

    path('ticket/<int:pk>/assignment/',
         AssignmentCreate,
         name='helpdesk_assignment_create_urlpattern')

]


