from __future__ import unicode_literals
from itertools import chain

from django.db import migrations


def populate_permissions_lists(apps):
    permission_class = apps.get_model('auth', 'Permission')

    ticket_permissions = permission_class.objects.filter(content_type__app_label='helpdesk',
                                                             content_type__model='ticket')

    perm_view_ticket = permission_class.objects.filter(content_type__app_label='helpdesk',
                                                           content_type__model='ticket',
                                                           codename='view_ticket')

    hd_user_permissions = chain(
                                perm_view_ticket,
                                ticket_permissions)

    hd_staff_permissions = chain(
                                perm_view_ticket,
                                ticket_permissions)

    hd_manager_permissions = chain(
                                    perm_view_ticket,
                                    ticket_permissions)

    my_groups_initialization_list = [
        {
            "name": "hd_user",
            "permissions_list": hd_user_permissions,
        },
        {
            "name": "hd_staff",
            "permissions_list": hd_staff_permissions,
        },
        {
            "name": "hd_admin",
            "permissions_list": hd_manager_permissions,
        },
    ]
    return my_groups_initialization_list


def add_group_permissions_data(apps, schema_editor):
    groups_initialization_list = populate_permissions_lists(apps)

    Group = apps.get_model('auth', 'Group')
    for group in groups_initialization_list:
        if group['permissions_list'] is not None:
            group_object = Group.objects.get(
                name=group['name']
            )
            group_object.permissions.set(group['permissions_list'])
            group_object.save()


def remove_group_permissions_data(apps, schema_editor):
    groups_initialization_list = populate_permissions_lists(apps)

    Group = apps.get_model('auth', 'Group')
    for group in groups_initialization_list:
        if group['permissions_list'] is not None:
            group_object = Group.objects.get(
                name=group['name']
            )
            list_of_permissions = group['permissions_list']
            for permission in list_of_permissions:
                group_object.permissions.remove(permission)
                group_object.save()


class Migration(migrations.Migration):
    dependencies = [
        ('helpdesk', '0006_create_groups'),
    ]

    operations = [
        migrations.RunPython(
            add_group_permissions_data,
            remove_group_permissions_data
        )
    ]
