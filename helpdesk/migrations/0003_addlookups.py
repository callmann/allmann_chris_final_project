
from django.db import migrations, models


CATEGORIES = [
    {
        'category_sort': 0,
        'category': 'Not Specified',
    },
    {
        'category_sort': 1,
        'category': 'Application/Software',
    },
    {
        'category_sort': 2,
        'category': 'Desktop Support',
    },
    {
        'category_sort': 3,
        'category': 'Hardware',
    },
    {
        'category_sort': 4,
        'category': 'Project',
    },
    {
        'category_sort': 5,
        'category': 'Other',
    },
]

PRIORITIES = [
    {
        'priority_sort': 1,
        'priority': 'Critical',
    },
    {
        'priority_sort': 2,
        'priority': 'High',
    },
    {
        'priority_sort': 3,
        'priority': 'Medium',
    },
    {
        'priority_sort': 4,
        'priority': 'Low',
    },
]

STATUSES = [
    {
        'status_sort': 1,
        'status': 'New',
    },
    {
        'status_sort': 2,
        'status': 'Pending',
    },
    {
        'status_sort': 3,
        'status': 'Open',
    },
    {
        'status_sort': 4,
        'status': 'On Hold',
    },
    {
        'status_sort': 5,
        'status': 'Closed',
    },
    {
        'status_sort': 6,
        'status': 'Resolved',
    },
]


def remove_category_data(apps, schema_editor):
    category_class = apps.get_model('helpdesk', 'Category')
    for this_category in CATEGORIES:
        category_object = category_class.objects.get(
            category_sort=this_category['category_sort'],
            category=this_category['category']
        )
        category_object.delete()


def add_category_data(apps, schema_editor):
    category_class = apps.get_model('helpdesk', 'Category')
    for this_category in CATEGORIES:
        category_object = category_class.objects.create(
            category_sort=this_category['category_sort'],
            category=this_category['category']
        )


def remove_priority_data(apps, schema_editor):
    priority_class = apps.get_model('helpdesk', 'Priority')
    for this_priority in PRIORITIES:
        priority_object = priority_class.objects.get(
            priority_sort=this_priority['priority_sort'],
            priority=this_priority['priority']
        )
        priority_object.delete()


def add_priority_data(apps, schema_editor):
    priority_class = apps.get_model('helpdesk', 'Priority')
    for this_priority in PRIORITIES:
        priority_object = priority_class.objects.create(
            priority_sort=this_priority['priority_sort'],
            priority=this_priority['priority']
        )


def remove_status_data(apps, schema_editor):
    status_class = apps.get_model('helpdesk', 'Status')
    for this_status in STATUSES:
        status_object = status_class.objects.get(
            status_sort=this_status['status_sort'],
            status=this_status['status']
        )
        status_object.delete()


def add_status_data(apps, schema_editor):
    status_class = apps.get_model('helpdesk', 'Status')
    for this_status in STATUSES:
        status_object = status_class.objects.create(
            status_sort=this_status['status_sort'],
            status=this_status['status']
        )


class Migration(migrations.Migration):

    dependencies = [
        ('helpdesk', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('category_id', models.AutoField(primary_key=True, serialize=False)),
                ('category', models.CharField(max_length=50, unique=True)),
                ('category_sort', models.IntegerField(default=99)),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Category',
                'ordering': ['category_sort'],
            },
        ),
        migrations.CreateModel(
            name='Priority',
            fields=[
                ('priority_id', models.AutoField(primary_key=True, serialize=False)),
                ('priority', models.CharField(max_length=50, unique=True)),
                ('priority_sort', models.IntegerField(default=99)),
            ],
            options={
                'verbose_name': 'Priority',
                'verbose_name_plural': 'Priority',
                'ordering': ['priority_sort'],
            },
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('status_id', models.AutoField(primary_key=True, serialize=False)),
                ('status', models.CharField(max_length=50, unique=True)),
                ('status_sort', models.IntegerField(default=99)),
            ],
            options={
                'verbose_name': 'Status',
                'verbose_name_plural': 'Status',
                'ordering': ['status_sort'],
            },
        ),

        migrations.DeleteModel(
            name='RequestStatus',
        ),

        migrations.RunPython(
            add_category_data,
            remove_category_data
        ),

        migrations.RunPython(
            add_priority_data,
            remove_priority_data
        ),

        migrations.RunPython(
            add_status_data,
            remove_status_data
        ),

    ]
