from django.contrib import admin
from .models import Status, Priority, Category, Profile, Ticket, Comment, Assignment

admin.site.register(Status)
admin.site.register(Priority)
admin.site.register(Category)
admin.site.register(Profile)
admin.site.register(Ticket)
admin.site.register(Comment)
admin.site.register(Assignment)