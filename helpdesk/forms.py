from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from helpdesk.fields import UserModelChoiceField
from helpdesk.models import Ticket, Comment, Assignment


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name','last_name','password1', 'password2', )


class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ('title','description','category','priority',)

    def clean_ticket_title(self):
        return self.cleaned_data['title'].strip()

    def clean_ticket_description(self):
        return self.cleaned_data['description'].strip()


class TicketFormStatus(forms.ModelForm):
    user = UserModelChoiceField(queryset=User.objects.all(),label="Requestor")

    class Meta:
        model = Ticket
        fields = ('title','status','user','description','category','priority',)

    def clean_ticket_title(self):
        return self.cleaned_data['title'].strip()

    def clean_ticket_description(self):
        return self.cleaned_data['description'].strip()


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('comment_text',)


class AssignmentForm(forms.ModelForm):
    class Meta:
        model = Assignment
        fields = ('agent',)
