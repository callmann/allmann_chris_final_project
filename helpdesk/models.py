from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Status(models.Model):
    status_id = models.AutoField(primary_key=True)
    status = models.CharField(max_length=50,unique=True,default='New')
    status_sort = models.IntegerField(default=99)

    def __str__(self):
        return '%s' % self.status

    class Meta:
        verbose_name = 'Status'
        verbose_name_plural = 'Status'
        ordering = ['status_sort']


class Priority(models.Model):
    priority_id = models.AutoField(primary_key=True)
    priority = models.CharField(max_length=50,unique=True,default='Low')
    priority_sort = models.IntegerField(default=99)

    def __str__(self):
        return '%s' % self.priority

    class Meta:
        verbose_name = 'Priority'
        verbose_name_plural = 'Priority'
        ordering = ['-priority_sort']


class Category(models.Model):
    category_id = models.AutoField(primary_key=True)
    category = models.CharField(max_length=50,unique=True)
    category_sort = models.IntegerField(default=99)

    def __str__(self):
        return '%s' % self.category

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Category'
        ordering = ['category_sort']


class Ticket(models.Model):
    ticket_id = models.AutoField(primary_key=True)
    ticket_num = models.IntegerField(blank=True,null=True)
    title = models.CharField(max_length=120, verbose_name='Summary')
    description = models.TextField(max_length=400)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Requestor')
    category = models.ForeignKey(Category, related_name='tickets', on_delete=models.PROTECT, null=True)
    priority = models.ForeignKey(Priority, related_name='tickets', on_delete=models.PROTECT, null=True)
    status = models.ForeignKey(Status, related_name='tickets', on_delete=models.PROTECT, null=True)

    def __str__(self):
        return '%s - %s' % (self.ticket_num, self.title)

    def get_absolute_url(self):
        return reverse('helpdesk_ticket_detail_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_update_url(self):
        return reverse('helpdesk_ticket_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_delete_url(self):
        return reverse('helpdesk_ticket_delete_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['date_created']
        permissions = [
            ("hd_admin", "Admins have full access to tickets."),
            ("hd_staff", "Staff has create and edit rights for tickets."),
        ]


class Comment(models.Model):
    comment_id = models.AutoField(primary_key=True)
    ticket = models.ForeignKey('Ticket', on_delete=models.CASCADE, related_name='comments', verbose_name='Ticket')
    comment_user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='User')
    comment_text = models.TextField(verbose_name='Comment')
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.comment_text

    def get_absolute_url(self):
        return reverse('helpdesk_comment_create_urlpattern_urlpattern',
                       kwargs={'pk': self.pk}
                       )




class Assignment(models.Model):
    assignment_id = models.AutoField(primary_key=True)
    ticket = models.ForeignKey('Ticket', on_delete=models.CASCADE, related_name='assignments', verbose_name=('Ticket'),)
    agent = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Agent')
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.agent.get_full_name()

    class Meta:
        ordering = ['-date_created']
        unique_together = (('ticket','agent'),)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)
    # other fields...

    def __str__(self):
        result = ''
        if self.user.first_name == '':
            result = '%s' % self.user
        else:
            result = '%s, %s' % (self.user.last_name, self.user.first_name)
        return result



@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


