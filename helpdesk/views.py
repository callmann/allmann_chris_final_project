from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes,force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.template.loader import render_to_string
from datetime import datetime
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from helpdesk.tokens import account_activation_token
from helpdesk.forms import SignUpForm, TicketForm, TicketFormStatus, CommentForm, AssignmentForm
from helpdesk.utils import PageLinksMixin,LimitAccessMixin
from .models import (
    Ticket,
    Comment,
)


@login_required
def home(request):
    return render(request, 'helpdesk/about.html')


class TicketList(LoginRequiredMixin,PermissionRequiredMixin,PageLinksMixin,ListView,LimitAccessMixin):
    paginate_by = 10
    model = Ticket
    permission_required = 'helpdesk.view_ticket'
    ordering = ['-date_created']

    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.user.has_perm('helpdesk.change_ticket') or self.request.user.has_perm('helpdesk.delete_ticket'):
            return qs
        return qs.filter(user=self.request.user)


class TicketDetail(LoginRequiredMixin,PermissionRequiredMixin,View):
    permission_required = 'helpdesk.view_ticket'
    model = Ticket


    def get(self, request, pk):
        ticket = get_object_or_404(
            Ticket,
            pk=pk
        )

        comments = Comment.objects.select_related('ticket').filter(ticket__ticket_id=ticket.ticket_id)

        return render(
            request,
            'helpdesk/ticket_detail.html',
            {'ticket': ticket,
             'comments':comments}

        )


class TicketCreate(LoginRequiredMixin,PermissionRequiredMixin,CreateView):
    permission_required = 'helpdesk.add_ticket'
    form_class = ''
    model = Ticket

    def get_form_class(self, queryset=None):
        if self.request.user.has_perm('helpdesk.change_ticket') or self.request.user.has_perm('helpdesk.delete_ticket'):
            return TicketFormStatus
        else:
            return TicketForm


    def form_valid(self, form):
        rn = int(datetime.utcnow().strftime("%y%m%d%H%M%S%f")[:14])
        form.instance.ticket_num = rn
        form.instance.user = self.request.user
        form.instance.status_id = 1
        return super().form_valid(form)


class TicketUpdate(LoginRequiredMixin,UpdateView):
    form_class= ''
    model = Ticket
    template_name = 'helpdesk/ticket_form_update.html'

    def get_form_class(self, queryset=None):
        if self.request.user.has_perm('helpdesk.change_ticket') or self.request.user.has_perm('helpdesk.delete_ticket'):
            return TicketFormStatus
        else:
            return TicketForm

    def get_object(self, queryset=None):
        obj = super().get_object()
        if obj.user == self.request.user or self.request.user.has_perm('helpdesk.change_ticket') or self.request.user.has_perm('helpdesk.delete_ticket'):
            return obj
        raise PermissionDenied


class TicketDelete(LoginRequiredMixin, DeleteView):

    form_class = TicketForm
    model = Ticket
    success_url = reverse_lazy('helpdesk_ticket_list_urlpattern')

    def get_object(self, queryset=None):
        obj = super().get_object()
        if obj.user == self.request.user or self.request.user.has_perm('helpdesk.change_ticket') or self.request.user.has_perm('helpdesk.delete_ticket'):
            return obj
        raise PermissionDenied


def CommentCreate(request, pk):
    ticket = get_object_or_404(Ticket, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.comment_user = request.user
            comment.ticket = ticket
            comment.save()
            return redirect('helpdesk_ticket_detail_urlpattern', pk=ticket.pk)
    else:
        form = CommentForm()
    return render(request, 'helpdesk/comment_form.html', {'form': form})


#not implemented
def AssignmentCreate(request, pk):
    ticket = get_object_or_404(Ticket, pk=pk)
    if request.method == "POST":
        form = AssignmentForm(request.POST)
        if form.is_valid():
            assignment = form.save(commit=False)
            assignment.agent = request.user
            assignment.ticket = ticket
            assignment.save()
            return redirect('helpdesk_ticket_detail_urlpattern', pk=ticket.pk)
    else:
        form = AssignmentForm()
    return render(request, 'helpdesk/assignment_form.html', {'form': form})


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = 'Activate your account'
            message = render_to_string('helpdesk/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': account_activation_token.make_token(user),
            })

            user.email_user(subject, message,'"IT Service Portal" <chris@uwm.foundation>',)

            return redirect('account_activation_sent')
    else:
        form = SignUpForm()
    return render(request, 'helpdesk/signup.html', {'form': form})


def account_activation_sent(request):
    return render(request, 'helpdesk/account_activation_sent.html')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        g = Group.objects.get(name='hd_user')
        g.user_set.add(user)
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('helpdesk_ticket_list_urlpattern')
    else:
        return render(request, 'helpdesk/account_activation_invalid.html')
